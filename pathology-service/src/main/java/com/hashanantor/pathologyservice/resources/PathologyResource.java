package com.hashanantor.pathologyservice.resources;

import java.util.Arrays;
import java.util.List;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hashanantor.pathologyservice.models.Disease;
import com.hashanantor.pathologyservice.models.DiseasesList;

@RestController
@RequestMapping("/pathology")
public class PathologyResource {

	private List<Disease> diseases = Arrays.asList(
			new Disease("D1", "Ashma", "Warm water bath"),
			new Disease("D2", "Headache", "Panadol capsule")
			
		);
	
	@RequestMapping("/diseases")
	public DiseasesList getDisease(){
		
		DiseasesList diseaseslist = new DiseasesList();
		diseaseslist.setDiseases(diseases);
		return diseaseslist;	
	}
	
	@RequestMapping("/diseases/{Id}")
	public Disease getDiseaseById (@PathVariable("Id") String Id) {
		
		Disease d = diseases.stream()
			.filter(disease -> Id.equals(disease.getId()))
			.findAny()
			.orElse(null);	
		return d;
	}	
}
