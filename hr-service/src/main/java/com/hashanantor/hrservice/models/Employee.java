package com.hashanantor.hrservice.models;

public class Employee {
	private String ID;
	private String firstname;
	private String lastname;
	private String specialty;
	
	public Employee() {
		
	}

	public Employee(String iD, String firstname, String lastname, String specialty) {
		
		ID = iD;
		this.firstname = firstname;
		this.lastname = lastname;
		this.specialty = specialty;
	}

	public String getID() {
		return ID;
	}

	public void setID(String iD) {
		ID = iD;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getSpecialty() {
		return specialty;
	}

	public void setSpecialty(String specialty) {
		this.specialty = specialty;
	}

		
}
