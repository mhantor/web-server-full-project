package com.hashanantor.hrservice.resources;

import java.util.Arrays;
import java.util.List;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hashanantor.hrservice.models.Employee;
import com.hashanantor.hrservice.models.EmployeesList;

@RestController
@RequestMapping("/hr")
public class HrResources {

	List<Employee> employees = Arrays.asList(
			new Employee("E1", "Kindson", "Munonye", "MedTech"),
			new Employee("E2", "Kate", "Winters", "Surgery"),
			new Employee("E3", "Lila", "Chucks", "Dentistry")
		);
	@RequestMapping("/employees")
	public EmployeesList getEmployees() {
		EmployeesList employeesList = new EmployeesList();
		employeesList.setEmployees(employees);
		return employeesList;
	}
	
	@RequestMapping("/employees/{Id}")
	public Employee getEmployeeById (@PathVariable("Id") String Id) {
		Employee e = employees.stream()
				.filter(employee ->Id.equals(employee.getID()))
				.findAny()
				.orElse(null);
		return e;
	}
}
